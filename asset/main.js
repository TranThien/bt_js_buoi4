function change() {
    var num1Element = document.getElementById("num-1").value * 1;
    var num2Element = document.getElementById("num-2").value * 1;
    var num3Element = document.getElementById("num-3").value * 1;

    var result = "";
    if (num1Element > num2Element && num1Element > num3Element) {
        result = `${num3Element} < ${num2Element} < ${num1Element}`;
    } else if (num1Element > num2Element && num3Element > num2Element) {
        result = `${num2Element} < ${num3Element} < ${num1Element}`;
    } else if (num1Element > num2Element && num3Element > num1Element) {
        result = `${num2Element} < ${num1Element} < ${num3Element}`;
    } else if (num2Element > num1Element && num2Element > num3Element) {
        result = `${num3Element} < ${num1Element} < ${num2Element}`;
    } else if (num2Element > num3Element && num3Element > num1Element) {
        result = `${num1Element} < ${num3Element} < ${num2Element}`;
    } else {
        result = `${num1Element} < ${num2Element} < ${num3Element}`;
    }
    var resultElement = document.getElementById("result");

    resultElement.innerHTML = result;
}

function hello() {
    var selectElement = document.getElementById("select");
    if (selectElement.value == "bo") {
        document.getElementById(
            "text"
        ).innerHTML = `Xin chào Hoàng Thượng đẹp trai 🧡🖤🧡💛💚💙`;
    } else if (selectElement.value == "me") {
        document.getElementById(
            "text"
        ).innerHTML = `Xin chào Hoàng Hậu xinh gái 🧡🖤🧡💛💚💙`;
    } else if (selectElement.value == "anh-trai") {
        document.getElementById(
            "text"
        ).innerHTML = `Xin chào anh trai xấu xa 🧡🖤🧡💛💚💙`;
    } else if (selectElement.value == "em-gai") {
        document.getElementById(
            "text"
        ).innerHTML = `Xin chào cô nương xinh đẹp của nhà ta 🧡🖤🧡💛💚💙`;
    } else {
        document.getElementById(
            "text"
        ).innerHTML = `Ngươi là ai mà dám đụng đến máy tính của tao , khôn hồn thì biến nhanh còn kịp🔪🔪🔪`;
    }
}

function count() {
    var value1Element = document.getElementById("txt-so-1").value * 1;
    var value2Element = document.getElementById("txt-so-2").value * 1;
    var value3Element = document.getElementById("txt-so-3").value * 1;

    var output1 = value1Element % 2 === 0 ? "Số chẵn" : "Số lẻ";
    var output2 = value2Element % 2 === 0 ? "Số chẵn" : "Số lẻ";
    var output3 = value3Element % 2 === 0 ? "Số chẵn" : "Số lẻ";

    var soChan = 0;
    var soLe = 0;
    if (output1 == "Số chẵn") {
        soChan += 1;
    } else {
        soLe += 1;
    }
    if (output2 == "Số chẵn") {
        soChan += 1;
    } else {
        soLe += 1;
    }
    if (output3 == "Số chẵn") {
        soChan += 1;
    } else {
        soLe += 1;
    }

    document.getElementById(
        "count-up"
    ).innerHTML = `Tổng có ${soChan} số chẵn và ${soLe} số lẻ`;
}

function guess() {
    var canhThuNhat = document.getElementById("txt-canh-1").value * 1;
    var canhThuHai = document.getElementById("txt-canh-2").value * 1;
    var canhThuBa = document.getElementById("txt-canh-3").value * 1;

    if (
        (canhThuNhat == canhThuHai && canhThuNhat == canhThuBa) ||
        (canhThuHai == canhThuNhat && canhThuHai == canhThuBa) ||
        (canhThuBa == canhThuNhat && canhThuBa == canhThuHai)
    ) {
        document.getElementById("content").innerHTML = `Đây là hình tam giác đều`;
    } else if (
        canhThuNhat == canhThuHai ||
        canhThuNhat == canhThuBa ||
        canhThuHai == canhThuBa
    ) {
        document.getElementById("content").innerHTML = `Đây là hình tam giác cân`;
    } else if (
        canhThuNhat * canhThuNhat ==
        canhThuHai * canhThuHai + canhThuBa * canhThuBa ||
        canhThuHai * canhThuHai ==
        canhThuNhat * canhThuNhat + canhThuBa * canhThuBa ||
        canhThuBa * canhThuBa == canhThuNhat * canhThuNhat + canhThuHai * canhThuHai
    ) {
        document.getElementById("content").innerHTML = `Đây là hình tam giác vuông`;
    } else {
        document.getElementById(
            "content"
        ).innerHTML = `Đây là hình tam giác nào đó`;
    }
}